# php-raw

php自身の機能を利用しフレームワークを使わずに作ってみる。


## 最初に参考にしたサイト

* [library/php - Docker Hub](https://hub.docker.com/_/php/)
* [DockerでPHP7.0×Apacheの環境を構築する（更新: 2017/6/27） - Qiita](https://qiita.com/kurkuru/items/fa7401a01c4d5dd98e4a)
* [Docker for Windows で、ホストのファイルが見えない！ - Qiita](https://qiita.com/chimatter/items/5e2768baeb8983525602)


## コンテナの作成

```
> docker run -d --name php-raw -p 80:80 -v E:/Projects/tsuchinaga/webapi-test/php/raw:/var/www php:7.2-apache
```

`-p 80:80`はcontainerの80番をlocalhostの80番に当てる設定。

`-v E:/Projects/tsuchinaga/webapi-test/php/raw:/var/www`は:より前のホストのディレクトリと:より後のコンテナのディレクトリを共有する設定。

php/rawの中身は空っぽでしたが、コンテナを立ち上げると/var/www配下にあるhtmlディレクトリがホスト側に表示されるはずです

うまくディレクトリ共有できない場合は、Docker for WindowsのShared Drivesの設定を確認してください。

少し時間をおいて[localhost:80](http://localhost:80)にアクセスすると403が返されるはず。


## 動作確認

phpinfoを設置して動作確認。

```
$ pwd
/var/www/html

$ echo "<?php phpinfo();" > index.php
```

これでもう一度[localhost:80](http://localhost:80)にアクセスするとphpinfoが見れるはず。

もちろん、ディレクトリ共有の設定が生きていればわざわざechoせずにホストで作成したものを設置すればいいのですけどね。


## DocumentRootの変更

デフォルトだと*:80へのアクセスは/var/www/html配下になっている。

これを/var/www/publicに変更して、自前のプログラムを読み込んでもらえるようにする。

0. [コンテナ]設定ファイルのコピー

    ```
    $ cp /etc/apache2/sites-available/000-default.conf /var/www/
    ```

0. [ホスト]設定ファイルのバックアップと書き換え

    0. 000-default.confを複製して000-default.conf.orgとする
    0. 000-default.conf内にあるDocumentRootを/var/www/publicにする
    0. htmlディレクトリをpublicに変更する

0. [コンテナ]書き換えられた設定ファイルを設置

    ```
    $ cp /var/www/000-default.conf /etc/apache2/sites-available/000-default.conf
    $ cp /var/www/000-default.conf.org /etc/apache2/sites-available/000-default.conf.org
    $ diff /etc/apache2/sites-available/000-default.conf.org /etc/apache2/sites-available/000-default.conf
    12c12
    <       DocumentRoot /var/www/html
    ---
    >       DocumentRoot /var/www/public
    ```

0. .htaccessのrewrite_modを有効化

    ```
    $ a2enmod rewrite
    ```


0. apacheの再起動

    といってもコンテナ内からはできないので、コンテナ自体を再起動してしまう。

    ```
    > docker restart php-raw
    ```

0. [localhost:80](http://localhost:80)にアクセスし、/publicを参照していることを確認


## PDO pg_sqlのinstall

phpのextensionであるPDOとpdo_pgsqlをinstallする

0. libpq-devをinstall(ないとpdo_pgsqlのinstallで怒られた

    ```
    $ apt update
    $ apt install libpq-dev
    ```

0. pdoとpdo_pgsqlのinstall

    ```
    $ docker-php-ext-install pdo pdo_pgsql
    ```

0. 再起動後に確認

    ```
    $ exit

    > docker restart php-raw

    > docker exec -it php-raw bash

    $ php -i | grep pdo
    Additional .ini files parsed => /usr/local/etc/php/conf.d/docker-php-ext-pdo_pgsql.ini
    pdo_pgsql
    pdo_sqlite
    ```

## composerでautoload

    composer.pharとcomposer.jsonを設置して、autoloaderを用意する。
