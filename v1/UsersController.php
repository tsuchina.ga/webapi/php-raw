<?php
namespace v1;

use PDO;

class UsersController
{

    static private $dbh = null;
    static public $params = [];
    static public $method = '';
    static public $request = [];
    static public $response = [
        '_links' => [],
        'result' => false,
        'message' => '',
        'request' => [],
        '_embedded' => []
    ];

    /**
    *
    **/
    public function __construct ($params)
    {
        // リクエストの取得
        self::$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'OTHER';
        if (self::$method === 'GET') {
            self::$request = $_GET;
        } else {
            self::$request = json_decode(file_get_contents('php://input'), true);
        }

        // レスポンスの初期化
        self::$response['request'] = self::$request;
        self::$response['_links']['self'] = ['href' => $_SERVER['REQUEST_URI'], 'method' => self::$method];

        // DBハンドルの生成
        if (self::$dbh === null) {
            $dbn = 'pgsql:dbname=webapi host=172.17.0.2 port=5432';
            $user = 'postgres';
            $pass = '';
            $option = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => true,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ];

            self::$dbh = new \PDO($dbn, $user, $pass, $option);
        }

        // uriからのparamsを取得し、処理の実行
        self::$params = $params;

        switch (count($params)) {
            case 0:
                $this->rootAction();
                break;
            case 1:
                $this->rootIdAction();
                break;
        }
    }

    /**
    * @return array
    **/
    public function getResponse()
    {
        return self::$response;
    }

    /**
    * @param string
    * @return boolean
    **/
    private function checkMethod($method)
    {
        $return = true;
        self::$response['message'] = '';
        self::$response['_embedded'] = [];

        if ($method !== self::$method) {
            $return = false;
            self::$response['message'] = '指定されたエンドポイントが存在しません';
            self::$response['_embedded'] = [
                'error' => self::$method.'メソッドは許可されていません'
            ];
        }
        return $return;
    }

    /**
    * method
    *   GET  : ユーザー一覧取得
    *   POST : ユーザー追加
    * endpoint /v1/users
    *
    **/
    private function rootAction()
    {
        if ($this->checkMethod('GET')) {
            $sql = "";
            $sql .= " SELECT * FROM users WHERE is_deleted = 0 ";

            $sth = self::$dbh->prepare($sql);
            $res = $sth->execute();
            $data = $sth->fetchAll();

            self::$response['result'] = true;
            self::$response['message'] = 'success';
            self::$response['_embedded'] = ['users' => $data];
        } elseif ($this->checkMethod('POST')) {
            $name = isset(self::$request['name']) ? self::$request['name'] : '';
            $age = isset(self::$request['age']) ? (int)self::$request['age'] : 20;

            $sql = "";
            $sql .= " INSERT INTO users (name, age) VALUES (:name, :age) ";
            $sth = self::$dbh->prepare($sql);
            $sth->bindValue(':name', $name, PDO::PARAM_STR);
            $sth->bindValue(':age', $age, PDO::PARAM_INT);
            $res = $sth->execute();
            if ($res) {
                self::$response['result'] = true;
                self::$response['message'] = 'success';
                self::$response['_embedded'] = [
                    'id' => self::$dbh->lastInsertId()
                ];
            } else {
                self::$response['result'] = false;
                self::$response['message'] = 'ユーザーの追加に失敗しました。';
            }
        }
    }

    /**
    * method
    *  GET:    指定されたユーザーIDを持つユーザーの詳細
    *  PUT:    ユーザー情報の更新
    *  DELETE: ユーザーの削除
    * endpoint /v1/users/{id}
    **/
    private function rootIdAction()
    {
        $userid = (int)self::$params[0];
        $userid = $userid >= 0 ? $userid : 0;

        if ($this->checkMethod('GET')) {
            $sql = "";
            $sql .= " SELECT * FROM users WHERE id = :id ";

            $sth = self::$dbh->prepare($sql);
            $sth->bindValue(':id', $userid, PDO::PARAM_INT);
            $res = $sth->execute();
            $data = $sth->fetchAll();

            self::$response['result'] = true;
            self::$response['message'] = isset($data[0]) ? 'success' : '該当するユーザーはいませんでした';
            self::$response['_embedded'] = [
                'user' => isset($data[0]) ? $data[0] : []
            ];
        } elseif ($this->checkMethod('PUT')) {
            $name = isset(self::$request['name']) ? self::$request['name'] : '';
            $age = isset(self::$request['age']) ? (int)self::$request['age'] : -1;

            $sql = "";
            $sql .= " UPDATE users SET updated_at = CURRENT_TIMESTAMP ";
            $sql .= " , name = ".($name !== '' ? ':' : '')."name ";
            $sql .= " , age = ".($age >= 0 ? ':' : '')."age ";
            $sql .= " WHERE id = :id ";

            $sth = self::$dbh->prepare($sql);
            $sth->bindValue(':id', $userid, PDO::PARAM_INT);
            $sth->bindValue(':name', $name, PDO::PARAM_STR);
            $sth->bindValue(':age', $age, PDO::PARAM_INT);
            $res = $sth->execute();
            if ($res) {
                self::$response['result'] = true;
                self::$response['message'] = 'success';
                self::$response['_embedded'] = [
                    'id' => $userid
                ];
            } else {
                self::$response['result'] = false;
                self::$response['message'] = 'ユーザー情報の更新に失敗しました。';
            }
        } elseif ($this->checkMethod('DELETE')) {
            $sql = "";
            $sql .= " UPDATE users SET updated_at = CURRENT_TIMESTAMP ";
            $sql .= " , is_deleted = 1 ";
            $sql .= " WHERE id = :id ";
            $sth = self::$dbh->prepare($sql);
            $sth->bindValue(':id', $userid, PDO::PARAM_INT);
            $res = $sth->execute();
            if ($res) {
                self::$response['result'] = true;
                self::$response['message'] = 'success';
                self::$response['_embedded'] = [
                    'id' => $userid
                ];
            } else {
                self::$response['result'] = false;
                self::$response['message'] = 'ユーザーの削除に失敗しました。';
            }
        }
    }
}
