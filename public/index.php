<?php

require_once '../vendor/autoload.php';

ini_set('display_errors', '1'); // デバッグ用
ini_set('error_reporting', E_ALL);
ini_set('error_log', '../logs/'.date('Ymd').'.error');
ini_set('log_errors', 'On');
date_default_timezone_set('Asia/Tokyo');

// WebAPIとして必要になる設定
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, X-Requested-With, Authorization');
header('Content-Type: application/hal+json; charset=utf-8');

// 返す内容はここに配列でいれる
$response = [
    '_links' => ['self' => ['href' => $_SERVER['REQUEST_URI'], 'method' => $_SERVER['REQUEST_METHOD']]],
    'result' => false,
    'message' => ''
];

$url = isset($_SERVER['REQUEST_URI']) ? trim($_SERVER['REQUEST_URI']) : '';
list($url, ) = explode('?', $url);
$url = trim($url, '/');
$params = explode('/', $url);

$version = array_shift($params);
$class = array_shift($params);

if ($version !== null || $class !== null) {
    $class = ucfirst(mb_strtolower($class)).'Controller';

    if (file_exists('../'.$version.'/'.$class.'.php')) {
        $class = '\\'.str_replace('.', '', $version).'\\'.$class;
        $instance = new $class($params);
        $response = $instance->getResponse();
    } else {
        $response['message'] = '指定されたエンドポイントが存在しません。';
    }
} else {
    $response['message'] = '指定されたエンドポイントが存在しません。';
}



echo json_encode($response, JSON_NUMERIC_CHECK|JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
